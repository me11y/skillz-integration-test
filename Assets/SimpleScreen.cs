﻿using UnityEngine;
using UnityEngine.UI;

namespace TestGame
{

    [RequireComponent(typeof(Canvas))]
    [RequireComponent(typeof(GraphicRaycaster))]
    public class SimpleScreen : MonoBehaviour
    {
        private Canvas _canvas;
        private GraphicRaycaster _raycaster;

        private void Start()
        {
            _canvas = GetComponent<Canvas>();
            _raycaster = GetComponent<GraphicRaycaster>();
        }

        public void SetScreenActive(bool isActive)
        {
            _canvas.enabled = isActive;
            _raycaster.enabled = isActive;
        }
    }
}