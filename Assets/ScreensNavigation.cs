﻿using UnityEngine;

namespace TestGame
{
    public class ScreensNavigation : MonoBehaviour
    {
        [SerializeField] private SimpleScreen[] _screens;
        private int _currentScreenIndex = 0;

        public void Navigate(int nextScreenIndex)
        {
            if(nextScreenIndex >= _screens.Length)
            {
                Debug.LogError("Wrong screen index");
                return;
            }

            _screens[_currentScreenIndex].SetScreenActive(false);
            _currentScreenIndex = nextScreenIndex;
            _screens[_currentScreenIndex].SetScreenActive(true);
        }
    }
}