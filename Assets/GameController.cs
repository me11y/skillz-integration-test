﻿using UnityEngine;

namespace TestGame
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private ScoreCounter _scoreCounter;
        [SerializeField] private GameTimer _timer;
        [SerializeField] private int _timerValue;
        [SerializeField] private UserInput _userInput;
        [SerializeField] private ResultPresenter _results;
        [SerializeField] private ScreensNavigation _navigation;

        private void OnEnable()
        {
            _timer.TimerEnded += EndGame;
        }

        private void OnDisable()
        {
            _timer.TimerEnded -= EndGame;
        }

        private void Start()
        {
            ResetGame();
        }

        public void ResetGame()
        {
            _userInput.CanClick = false;
            _scoreCounter.ResetScore();
            _timer.ResetTimer();
        }

        public void StartGame()
        {
            _userInput.CanClick = true;
            _timer.StartTimer(_timerValue);
        }

        public void EndGame()
        {   
            _navigation.Navigate(2);
            _results.SetResult(_scoreCounter.Score);
        }

        public void SubmitResults()
        {
            SkillzController.Instance.ReportResults(_scoreCounter.Score);   
            ResetGame();
        }

        public void LeaveGame()
        {
            ResetGame();
            SkillzController.Instance.AbortMatch();
        }

    }
}