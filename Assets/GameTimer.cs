﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace TestGame
{
    public class GameTimer : MonoBehaviour
    {
        private int _secondsLeft;
        public UnityAction<int> TimerTicked;
        public UnityAction TimerEnded;
        public void StartTimer(int secondsTotal)
        {
            _secondsLeft = secondsTotal;
            StartCoroutine(Tick());
        }

        public void ResetTimer()
        {
            StopAllCoroutines();
        }

        private IEnumerator Tick()
        {
            var waitFor1s = new WaitForSeconds(1);
            TimerTicked?.Invoke(_secondsLeft);
            while (_secondsLeft > 0)
            {
                yield return waitFor1s;
                _secondsLeft--;
                TimerTicked?.Invoke(_secondsLeft);
            }
            TimerEnded?.Invoke();
        }
    }
}