﻿using UnityEngine;
using UnityEngine.Events;

namespace TestGame
{
    public class ScoreCounter : MonoBehaviour
    {
        public int Score { get; private set; } = 0;
        public UnityAction<int> ScoreChanged;

        public void AddScore()
        {
            Score++;
            ScoreChanged?.Invoke(Score);
        }

        public void ResetScore()
        {
            Score = 0;
            ScoreChanged?.Invoke(Score);
        }
    }
}