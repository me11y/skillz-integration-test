﻿using UnityEngine;

namespace TestGame
{
    public class SkillzController : Singleton<SkillzController>
    {
        public void LaunchSkillz()
        {
            SkillzCrossPlatform.LaunchSkillz(new SkillzImpl());
        }

        public void AbortMatch()
        {
            SkillzCrossPlatform.AbortMatch();
        }

        public void ReportResults(int score)
        {
            SkillzCrossPlatform.ReportFinalScore(score);
        }
    }
}