﻿using UnityEngine;

namespace TestGame {

    public class UserInput : MonoBehaviour
    {
        [SerializeField] private ScoreCounter _scoreCounter;
        public bool CanClick { get; set; }
        public void Click()
        {
            if (CanClick)
            {
                _scoreCounter.AddScore();
            }
        }

    }
}