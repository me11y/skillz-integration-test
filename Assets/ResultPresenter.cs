﻿using UnityEngine;
using UnityEngine.UI;

public class ResultPresenter : MonoBehaviour
{
    private Text _resultScore;

    private void Start()
    {
        _resultScore = GetComponent<Text>();
    }

    public void SetResult(int score)
    {
        _resultScore.text = score.ToString();
    }
}
