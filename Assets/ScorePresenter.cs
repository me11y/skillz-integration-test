﻿using UnityEngine;
using UnityEngine.UI;

namespace TestGame
{
    [RequireComponent(typeof(Text))]
    public class ScorePresenter : MonoBehaviour
    {
        [SerializeField] private ScoreCounter _counter;
        private Text _scoreView;

        private const string LABEL = "Score: ";

        private void OnEnable()
        {
            _counter.ScoreChanged += UpdateView;
        }

        private void OnDisable()
        {
            _counter.ScoreChanged -= UpdateView;
        }

        private void Start()
        {
            _scoreView = GetComponent<Text>();
        }

        private void UpdateView(int score)
        {
            _scoreView.text = LABEL + score;
        }
    }
}