﻿using System;
using UnityEngine;
using UnityEngine.UI;
namespace TestGame {

    [RequireComponent(typeof(Text))]
    public class TimerPresenter : MonoBehaviour
    {
        [SerializeField] private GameTimer _gameTimer;
        private Text _timerView;

        private void OnEnable()
        {
            _gameTimer.TimerTicked += UpdateView;
        }

        private void OnDisable()
        {
            _gameTimer.TimerTicked -= UpdateView;
        }

        private void Start()
        {
            _timerView = GetComponent<Text>();
        }

        private void UpdateView(int seconds)
        {
            var time = TimeSpan.FromSeconds(seconds);
            _timerView.text = time.ToString(@"hh\:mm\:ss");
        }
    }
}