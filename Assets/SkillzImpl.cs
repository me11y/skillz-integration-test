﻿using SkillzSDK;
using UnityEngine.SceneManagement;

namespace TestGame
{
    public class SkillzImpl : SkillzMatchDelegate
    {
        public void OnMatchWillBegin(Match matchInfo)
        {
            SceneManager.LoadScene(1);              
        }

        public void OnSkillzWillExit()
        {

        }

    }
}
